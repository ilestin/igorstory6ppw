from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import Form
from .views import profile
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from django.utils import timezone
from selenium.webdriver.support.color import Color
from selenium import webdriver
import datetime


# Create your tests here.
class story6ppwUnitTest(TestCase):
    def test_lab6_landing_page_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    #def test_lab6_using_index_func(self):
        #found = resolve('/story6ppw')
        #self.assertEqual(found.func, index)

    def test_lab6_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        Status.objects.create(status='senang')
        counting = Status.objects.all().count()
        self.assertEqual(counting, 1)
		

    def test_form_status_has_placeholder_and_css_classes_and_maximum_300_character(self):
        form = Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('id="id_status"', form.as_p())
        self.assertIn('maxlength="300"', form.as_p())

    def test_form_validation_max_300_character(self):
        form = Form(data={'status': 'x'*310})
        self.assertFalse(form.is_valid())

    #def test_lab6_post_success_and_render_the_result(self):
    #    test = 'hehehe'
    #    response_post = Client().post('/story6ppw', {'status': 'hehehe'})
    #    self.assertEqual(response_post.status_code, 302)

        #response = Client().get('/story6ppw')
        #html_response = response.content.decode('utf8')
        #self.assertIn(test, html_response)
		
    #def test_lab6_url_is_exist(self):
    #    response = Client().get('/story6ppw')
    #    self.assertEqual(response.status_code, 200)

    def test_profile_using_profile_func(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    #def test_profile_has_profile_and_css_classes(self):
    #    testnama = 'Nama:'
    #    testclass = "class='rounded isi'"
    #    response = Client().get('/profile')
    #    html_response = response.content.decode('utf8')
    #    self.assertIn(testnama, html_response)
    #    self.assertIn(testclass, html_response)	

class story6ppwFunctionalTest(TestCase):
     def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        super(story6ppwFunctionalTest, self).setUp()

     def tearDown(self):
        self.selenium.quit()
        super(story6ppwFunctionalTest, self).tearDown()


     def test_input_todo(self):
        driver = self.driver
        #driver = webdriver.Chrome()
        driver.get('http://localhost:8000/')
        time.sleep(2)
        search_box = driver.find_element_by_name('status')
        search_box.send_keys('coba coba')
        search_box.submit()
        self.assertIn('coba coba',driver.page_source)
        time.sleep(5)

     def BasicFunctionalTest(self):
        driver = self.driver
        driver.get('http://story6ppwigor.herokuapp.com/')  # Opening the page
        text = driver.find_element_by_tag_name('h1').text # find the form element
        self.asserrtIn('Hello, Apa kabar?',text)    # find the form element
        button = driver.find_element_by_tag_name('button').text
        self.asserrtIn('Submit', button)
        colorbutton = driver.find_element_by_tag_name('button').value_of_css_property('background-color')
        convertcolor = Color.from_string(btn).hex
        self.assertEqual('#007bff', color)
        navbar = driver.find_element_by_tag_name('nav').value_of_css_property('background-color')
        navbarcolor = Color.from_string(navbar).hex
        self.assertEqual('#343a40', navbarcolor)

     def test_if_page_has_greetings(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        assert selenium.page_source.find("Halo, Apa kabar?")

     def test_page_has_footer(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        footer = selenium.find_element_by_class_name('footer').text
        self.assertIn("Ihsan M. Aulia", footer)

     def test_footer_in_the_right_position(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        footer = selenium.find_element_by_class_name(
            'footer').value_of_css_property("position")
        self.assertEqual('absolute', footer)

     def test_footer_has_the_right_colour(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        footer = selenium.find_element_by_class_name(
            'footer').value_of_css_property("color")
        self.assertEquals('rgba(115, 116, 143, 1)', footer)

     def test_footer_text_has_the_right_colour(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        footer_text = selenium.find_element_by_class_name(
            'text').value_of_css_property("color")
        self.assertEquals('rgba(115, 116, 143, 1)', footer_text)



