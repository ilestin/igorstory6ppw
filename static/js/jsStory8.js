$(document).ready(function () {
    $("#button-orange").click(function () {
        $(".body").css("background-color", "#FF8C00"); //edit, body must be in quotes!
        $(".body").css("color", "#ffffff"); //edit, body must be in quotes!
    });
});
$(document).ready(function () {
    $("#button-green").click(function () {
        $(".body").css("background-color", "#2E8B57"); //edit, body must be in quotes!
        $(".body").css("color", "#ffffff"); //edit, body must be in quotes!
    });
});
$(document).ready(function () {
    $("#button-default").click(function () {
        $(".body").css("background-color", "#f5f5f5"); //edit, body must be in quotes!
        $(".body").css("color", "#73748F"); //edit, body must be in quotes!
    });
});
$(document).ready(function ($) {
    $('#accordion').find('.card-link').click(function () {

        //Expand or collapse this panel
        $(this).next().slideToggle('fast');

        //Hide the other panels
        $(".accordion-content").not($(this).next()).slideUp('fast');

    });
});